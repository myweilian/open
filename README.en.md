<p align="center">
    <a href="http://weilian.xin/" target="_blank">
        <img src="https://img.weilian.xin/image/2022/09/63ffac3dbeded9a842e69ea75194e426.png" height="100px">
    </a>
    <h1 align="center">Wiripples Open Platform SDK</h1>
    <br/>
</p>

## Introduce
weilian open It IS A SOFTWARE TOOL FOR MICRO-RIPPLE OPEN PLATFORM INTERFACE DOCKING SOLUTION, for THE realization of rapid access to merchants, simple development

## Composer Install Command

    composer require weiliannet/open

## Parameter Configuration Description

```php
$config = [
    'version' => '0.0.1',           // version of the interface used
    'merchant_no' => 'your merchant number',
    'tid' => 'your device number',
    'appid' => 'your appId',
    'appkey' => 'your appKey',
    'http' => [                     // network request configuration information
        'timeout' => 60             // timeout period of the request
    ]
];
$app = Factory::Virtual($config);
```

## Business flow chart

![Alt text](https://img.weilian.xin/image/2022/09/d4847f69a84996a140ca17dca8cbcba0.png)

## 业务调用示例

1. 获取商品类型列表：$app->goods_official->getTypeList();
2. 获取商品列表：$app->goods_official->getList();
3. 获取单个商品：$app->goods_official->getDetails();
4. 创建订单购买商品：$app->goods_official->buy();
5. 获取订单状态列表：$app->order_goods->getStatusList();
6. 获取订单列表：$app->order_goods->getList();
7. 获取单个订单：$app->order_goods->getDetails();
8. 获取账单列表：$app->user_balance->getList();
9. 查询商户余额：$app->user_balance->getValue();

## 工具调用示例

1. 对数据进行解密：$app->security->decode();
2. 对数据进行加密：$app->security->encode();

## 提交参数和返回参数

    提交参数和返回参数字段详解请点击下方的[文档地址]即可查看

#### [文档地址](http://weilian.xin/docking-document.pdf) | [官方网站](http://weilian.xin/) | [商户平台](http://weilian.xin/merchant)
