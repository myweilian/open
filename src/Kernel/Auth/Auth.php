<?php

namespace weiliannet\open\Kernel\Auth;

class Auth
{
	private $_cache;

	private $_request;

    protected $_config;

    public function __construct($app)
    {
        $this->_cache = $app['cache'];
        $this->_request = $app['request'];
        $this->_config = $app['config'];
        date_default_timezone_set("PRC");
    }

    /**
     * [getToken 获取令牌]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:35:42+0800
     * @return   [type]                   [description]
     */
    public function getToken()
    {
    	// 获取缓存中的令牌
    	$cache = $this->getCacheToken();
    	// 判断缓存中是否令牌
        if (empty($cache)) {
            $res = $this->requestToken()['data'];
            $cache = $this->getCacheToken();
        } else {
            $res = $cache[0];
        }

    	// 判断缓存中令牌组是否可以刷新
    	if (!empty($cache) && count($cache) <= 1 && strtotime($cache[0]['expiretime']) - 150 < time()) {
            $this->requestRefreshToken();
        }
    	return $res['token'];
    }

    /**
     * [getCacheToken 获取缓存中的令牌]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:35:49+0800
     * @return   [type]                   [description]
     */
    private function getCacheToken()
    {
    	$cache = $this->_cache->getArrayValue('access-token');
    	if (empty($cache)) return [];
    	foreach ($cache as $key => $value) {
    		if (strtotime($value['expiretime']) <= time()) unset($cache[$key]);
    	}
    	$cache = array_values($cache);
    	return $cache;
    }

    /**
     * [setCacheToken 设置令牌缓存]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:35:58+0800
     * @param    [type]                   $data [description]
     */
    private function setCacheToken($data)
    {
    	$data = array_values($data);
    	$cache = $this->_cache->setArrayValue('access-token', $data);
        if (!$cache) {
            throw new \Exception("储存令牌失败");
        } else {
            return true;
        }
    }

    /**
     * [requestCode 请求获取授权码]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:36:07+0800
     * @return   [type]                   [description]
     */
    public function requestCode()
    {
    	$res = $this->_request->getJson('login/get-code', [
    		'appid' => $this->_config->getAppid()
    	],[
    		'headers' => $this->getHeaders()
    	]);
        if ($res['error'] < 0) {
            throw new \Exception($res['message'], $res['error']);
        } else {
            return $res['data']['code'];
        }
    }

    /**
     * [requestToken 请求获取令牌]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:36:16+0800
     * @return   [type]                   [description]
     */
    public function requestToken()
    {
    	$code = $this->requestCode();
    	$res = $this->_request->getJson('login/get-token', [
    		'code' => $code
    	],[
    		'headers' => $this->getHeaders()
    	]);
        if ($res['error'] < 0) {
            throw new \Exception($res['message'], $res['error']);
        } else {
            $cache = $this->getCacheToken();
            $cache[] = $res['data'];
            $this->setCacheToken($cache);

            return $res;
        }
    }

    /**
     * [requestRefreshToken 请求刷新令牌]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:36:27+0800
     * @return   [type]                   [description]
     */
    public function requestRefreshToken()
    {
    	$cache = $this->getCacheToken();
    	$res = $this->_request->getJson('login/refresh-token', [
    		'appid' => $this->_config->getAppid(),
    		'token' => $cache[0]['token'],
    	],[
    		'headers' => $this->getHeaders()
    	]);
        if ($res['error'] < 0) {
            throw new \Exception($res['message'], $res['error']);
        } else {
            $cache[] = $res['data']['refresh'];
            $this->setCacheToken($cache);

            return $res;
        }
    }

    /**
     * [getHeaders 设置请求头]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:36:36+0800
     * @return   [type]                   [description]
     */
    public function getHeaders()
    {
    	return [
    		'version' => $this->_config->getVersion()
    	];
    }
}