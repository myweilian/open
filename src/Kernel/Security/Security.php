<?php

namespace weiliannet\open\Kernel\Security;

class Security
{
    private $password;

    public function __construct($password)
    {
        $this->password = $password;
    }

    public function decode(string $data)
	{
		$openssl_data = base64_decode($data);
		if (empty($openssl_data)) throw new \Exception("数据不是base64的格式");
		$json_data = openssl_decrypt($openssl_data, 'AES-256-ECB', $this->password, OPENSSL_RAW_DATA);
		if (empty($json_data)) throw new \Exception("AES解密失败，参数不正确，请仔细核对规则");
		$res = json_decode($json_data, true);
		if (empty($res)) throw new \Exception("数据不是json的格式");
		return $res;
	}

	public function encode(array $data)
	{
		krsort($data);
		$json_data = json_encode($data);
		$openssl_data = openssl_encrypt($json_data, 'AES-256-ECB', $this->password, OPENSSL_RAW_DATA);
        return base64_encode($openssl_data);
	}
}