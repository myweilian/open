<?php

namespace weiliannet\open\Kernel\Providers;

use Pimple\Container;
use weiliannet\open\Kernel\Http\Request;

class RequestServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['request'] = function ($app) {
            return new Request($app->getConfig()['http']);
        };
    }
}