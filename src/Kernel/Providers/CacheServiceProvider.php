<?php

namespace weiliannet\open\Kernel\Providers;

use Pimple\Container;
use weiliannet\open\Kernel\Cache\Cache;

class CacheServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['cache'] = function () {
            return new Cache();
        };
    }
}