<?php

namespace weiliannet\open\Kernel\Providers;

use Pimple\Container;
use weiliannet\open\Kernel\Config\Config;

class ConfigServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['config'] = function ($app) {
            $config = $app->getConfig();
            return new Config($config['version'], $config['merchant_no'], $config['appid'], $config['appkey'], $config['http'], $config['tid']);
        };
    }
}