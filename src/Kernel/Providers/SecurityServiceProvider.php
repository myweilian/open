<?php

namespace weiliannet\open\Kernel\Providers;

use Pimple\Container;
use weiliannet\open\Kernel\Security\Security;

class SecurityServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['security'] = function ($app) {
            return new Security($app['config']->getAppkey());
        };
    }
}