<?php

namespace weiliannet\open\Kernel\Business\Virtual;

use weiliannet\open\Kernel\Providers\CacheServiceProvider;
use weiliannet\open\Kernel\Providers\ConfigServiceProvider;
use weiliannet\open\Kernel\Providers\RequestServiceProvider;
use weiliannet\open\Kernel\Providers\SecurityServiceProvider;

class ServiceContainer extends \Pimple\Container
{
    protected $providers = [];

    protected $_config;

    public function __construct($config)
    {
        $this->_config = $config;
        $this->registerProviders();
    }

    public function __get($name)
    {
        // TODO: Implement __get() method.
        return $this->offsetGet($name);
    }

    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
        $this->offsetSet($name, $value);
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        $default = [
            'version' => '0.0.1',
            'http' => [
                'timeout' => 60,
                'base_uri' => 'http://weilian.xin/api/'
            ]
        ];

        return array_replace_recursive($default, $this->_config);
    }

    /**
     * @return array
     */
    public function getProviders()
    {
        return array_merge([
            CacheServiceProvider::class,
            ConfigServiceProvider::class,
            RequestServiceProvider::class,
            SecurityServiceProvider::class
        ], $this->providers);
    }

    public function registerProviders()
    {
        $providers = $this->getProviders();
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}