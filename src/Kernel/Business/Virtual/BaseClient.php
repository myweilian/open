<?php

namespace weiliannet\open\Kernel\Business\Virtual;

class BaseClient
{
    protected $_app;

    private $_request;

    protected $_config;

    protected $_security;

    public function __construct($app)
    {
        $this->_app = $app;
        $this->_request = $app['request'];
        $this->_config = $app['config'];
        $this->_security = $app['security'];
    }

    /**
     * [getHttp get请求]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:36:51+0800
     * @param    [type]                   $uri     [description]
     * @param    array                    $data    [description]
     * @param    array                    $options [description]
     * @return   [type]                            [description]
     */
    public function getHttp($uri, $data = [], $options = [])
    {
        $options = $this->getOptions($options);
        $res = $this->_request->getJson($uri, $data, $options);
        if (!empty($res['data'])) $res['data'] = $this->_security->decode($res['data']);
        return $res;
    }

    /**
     * [postHttp post请求]
     * @Author   张张嘴
     * @DateTime 2022-09-22T16:37:03+0800
     * @param    [type]                   $uri     [description]
     * @param    array                    $data    [description]
     * @param    array                    $options [description]
     * @return   [type]                            [description]
     */
    public function postHttp($uri, $data = [], $options = [])
    {
        $options = $this->getOptions($options);
        if (!empty($data)) $data['data'] = $this->_security->encode($data);
        $res = $this->_request->postJson($uri, $data, $options);
        if (!empty($res['data'])) $res['data'] = $this->_security->decode($res['data']);
        return $res;
    }

    /***/
    private function getOptions($data)
    {
        return array_replace_recursive($this->getHeaders(), $data);
    }

    /***/
    private function getHeaders()
    {
        return [
            'headers' => [
                'version' => $this->_config->getVersion(),
            ],
            'auth' => $this->getAuth()
        ];
    }

    /***/
    private function getAuth()
    {
        return [$this->_config->getAppid(), $this->_app['auth']->getToken()];
    }
}