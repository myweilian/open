<?php

namespace weiliannet\open\Kernel\Config;

class Config
{
    private $version;

    private $merchant_no;

    private $tid;

    private $appid;

    private $appkey;

    private $http;

    public function __construct($version, $merchant_no, $appid, $appkey, $http, $tid='')
    {
    	$this->setVersion($version);
    	$this->setMerchantNo($merchant_no);
    	$this->setAppid($appid);
    	$this->setAppkey($appkey);
    	$this->setHttp($http);
    	$this->setTid($tid);
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    private function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getMerchantNo()
    {
        return $this->merchant_no;
    }

    /**
     * @param mixed $merchant_no
     */
    private function setMerchantNo($merchant_no)
    {
        $this->merchant_no = $merchant_no;
    }

    /**
     * @return mixed
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * @param mixed $tid
     */
    private function setTid($tid='')
    {
        $this->tid = $tid;
    }

    /**
     * @return mixed
     */
    public function getAppid()
    {
        return $this->appid;
    }

    /**
     * @param mixed $appid
     */
    private function setAppid($appid)
    {
        $this->appid = $appid;
    }

    /**
     * @return mixed
     */
    public function getAppkey()
    {
        return $this->appkey;
    }

    /**
     * @param mixed $appkey
     */
    private function setAppkey($appkey)
    {
        $this->appkey = $appkey;
    }

    /**
     * @return mixed
     */
    public function getHttp()
    {
        return $this->http;
    }

    /**
     * @param mixed $http
     */
    private function setHttp($http)
    {
        $this->http = $http;
    }


}