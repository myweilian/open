<?php

namespace weiliannet\open\Kernel\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Request
{
    private $_client;

    public function __construct($config)
    {
        $this->_client = new Client($config);
    }

    private function request($method, $uri, array $options=[])
    {
        try {
            $response = $this->_client->request($method, $uri, $options);
            if ($response->getStatusCode() === 200) {
                return (string)$response->getBody();
            }
            throw new \Exception($response->getReasonPhrase(),$response->getStatusCode());
        }catch (RequestException $exception){
            throw new \Exception($exception->getMessage(),-$exception->getLine());
        }
    }

    private function arrayToQuery($data)
    {
        $res = '';
        if (empty($data)) return $res;
        foreach ($data as $key => $value) {
            $res .= "&{$key}={$value}";
        }
        return '?' . substr($res, 1);
    }

    public function getJson($uri, $data = [], $options = [])
    {
        $uri .= $this->arrayToQuery($data);
        $resp = $this->request("GET", $uri, $options);
        return json_decode($resp, true);
    }

    public function postJson($uri, $data = [], $options = [])
    {
        if (!empty($data)) $options['json'] = $data;
        $resp = $this->request("POST", $uri, $options);
        return json_decode($resp, true);
    }

    
}