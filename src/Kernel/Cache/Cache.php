<?php

namespace weiliannet\open\Kernel\Cache;

use Symfony\Component\Cache\Simple\FilesystemCache;

class Cache
{
    private $_cache;

    private $_key = 'weilianopen.';

    public function __construct()
    {
        $this->_cache = new FilesystemCache();
    }

    protected function getKey(string $namespace)
    {
        return $this->_key.$namespace;
    }

    public function getStringValue(string $namespace)
    {
        $key = $this->getKey($namespace);
        return $this->_cache->get($key);
    }

    public function setStringValue(string $namespace, string $data, int $duration = null)
    {
        $key = $this->getKey($namespace);
        return $this->_cache->set($key, $data, $duration);
    }

    public function getArrayValue(string $namespace)
    {
        $key = $this->getKey($namespace);
        return json_decode($this->_cache->get($key), true);
    }

    public function setArrayValue(string $namespace, array $data, int $duration = null)
    {
        $key = $this->getKey($namespace);
        return $this->_cache->set($key, json_encode($data), $duration);
    }

    public function delete(string $namespace)
    {
        $key = $this->getKey($namespace);
        return $this->_cache->delete($key);
    }

    public function clear()
    {
        return $this->_cache->clear();
    }
}