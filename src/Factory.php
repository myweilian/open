<?php

namespace weiliannet\open;

class Factory
{
    public static function make($filename, array $config)
    {
        $application = "\\weiliannet\\open\\{$filename}\\Application";
        try {
        	return new $application($config);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function __callStatic($method, $arguments)
    {
        return self::make($method, ...$arguments);
    }
}