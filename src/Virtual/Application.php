<?php

namespace weiliannet\open\Virtual;

class Application extends \weiliannet\open\Kernel\Business\Virtual\ServiceContainer
{
    protected $providers = [
        Auth\ServiceProvider::class,
        Goods\ServiceProvider::class,
        Order\ServiceProvider::class,
        User\ServiceProvider::class
    ];
}