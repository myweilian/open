<?php

namespace weiliannet\open\Virtual\Auth;

use Pimple\Container;

class ServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['auth'] = function ($app) {
            return new Client($app);
        };
    }
}