<?php

namespace weiliannet\open\Virtual\User;

use Pimple\Container;

class ServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['user_balance'] = function ($app) {
            return new Balance($app);
        };
    }
}