<?php

namespace weiliannet\open\Virtual\User;

class Balance extends \weiliannet\open\Kernel\Business\Virtual\BaseClient
{
	/**
	 * [getList 获取账单列表]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:48:05+0800
	 * @param    array                    $query [description]
	 * @return   [type]                          [description]
	 */
	public function getList($query = [])
	{
		return $this->getHttp('user/balance/index', $query);
	}

	/**
	 * [getValue 查询商户余额]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:48:13+0800
	 * @return   [type]                   [description]
	 */
	public function getValue()
	{
		return $this->getHttp('user/balance/query');
	}
}