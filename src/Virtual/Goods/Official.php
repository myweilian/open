<?php

namespace weiliannet\open\Virtual\Goods;

class Official extends \weiliannet\open\Kernel\Business\Virtual\BaseClient
{
	/**
	 * [getTypeList 获取商品类型列表]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:45:03+0800
	 * @return   [type]                   [description]
	 */
	public function getTypeList()
	{
		return $this->getHttp('goods/index/type-list');
	}

	/**
	 * [getList 获取商品列表]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:45:35+0800
	 * @param    array                    $query [description]
	 * @return   [type]                          [description]
	 */
	public function getList($query = [])
	{
		return $this->getHttp('goods/index/list', $query);
	}

	/**
	 * [getDetails 获取单个商品]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:45:43+0800
	 * @param    array                    $data [description]
	 * @return   [type]                         [description]
	 */
	public function getDetails($data)
	{
		return $this->postHttp('goods/index/one', $data);
	}

	/**
	 * [buy 创建订单购买商品]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:46:18+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function buy($data)
	{
		return $this->postHttp('goods/index/buy', $data);
	}
}