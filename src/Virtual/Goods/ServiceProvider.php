<?php

namespace weiliannet\open\Virtual\Goods;

use Pimple\Container;

class ServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['goods_official'] = function ($app) {
            return new Official($app);
        };
    }
}