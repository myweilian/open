<?php

namespace weiliannet\open\Virtual\Order;

use Pimple\Container;

class ServiceProvider implements \Pimple\ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        // TODO: Implement register() method.
        $pimple['order_goods'] = function ($app) {
            return new Goods($app);
        };
    }
}