<?php

namespace weiliannet\open\Virtual\Order;

class Goods extends \weiliannet\open\Kernel\Business\Virtual\BaseClient
{
	/**
	 * [getStatusList 获取订单状态列表]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:46:50+0800
	 * @return   [type]                   [description]
	 */
	public function getStatusList()
	{
		return $this->getHttp('order/goods/status-list');
	}

	/**
	 * [getList 获取订单列表]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:47:00+0800
	 * @param    array                    $query [description]
	 * @return   [type]                          [description]
	 */
	public function getList($query = [])
	{
		return $this->getHttp('order/goods/index', $query);
	}

	/**
	 * [getDetails 获取单个订单]
	 * @Author   张张嘴
	 * @DateTime 2022-09-22T11:47:36+0800
	 * @param    [type]                   $data [description]
	 * @return   [type]                         [description]
	 */
	public function getDetails($data)
	{
		return $this->postHttp('order/goods/one', $data);
	}
}